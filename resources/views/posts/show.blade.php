@extends('layouts.app')

@section('content')
<h1>{{$post->title}} </h1>
<small>Written on {{$post->created_at}} </small>

<div class='my-4'>
    {{$post->body}}
</div>

<!-- Modal -->
@if(!Auth::guest())
<button type='button' class="btn btn-primary" data-toggle="modal" data-target="#commentModal">Add a new comment</button>
@endif

<div class="modal fade" id="commentModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New comment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <form action="/posts/{{$post->id}}/comment" method='POST'>
        @csrf
        <div class="modal-body">
            <div class="form-group">
                <label for="message-text" class="col-form-label">Comment:</label>
                <textarea class="form-control" id="message-text" name='content'></textarea>
            </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Submit</button>
         </div>
    </form>
    </div>
  </div>
</div>

<h4 class='mt-4'>Comments:</h4>
<!-- Display comments -->
@if(count($comments)>0)
<!-- OR: $post->comments (because of model relationship) -->
    @foreach($comments as $comment)
    <div>
        <p class='mt-4'>{{$comment->content}}</p>
        <small  >Written by {{$comment->user->name}} on {{$comment->created_at}} </small>
    </div>
    @endforeach
@else
    <p>No comments.</p>
@endif

@endsection