@extends('layouts.app')

@section('content')
<h1>Create Post</h1>
<form action="{{action('PostController@store') }}" method='POST'>
    @csrf
    <!-- generates a CSRF token everytimee a form is submitted (security) -->
    <div class="form-group">
        <label for="title-input">Title</label>
        <input id='title-input' type="text" name='title' class='form-control' placeholder="Title">
    </div>
    <div class="form-group">
        <label for="body-input">Body</label>
        <textarea id="body-input" class='form-control' name='body' rows="5" placeholder="Body"></textarea>
    </div>

    <button type='submit' class='btn btn-primary'>Submit</button>
    <!-- when you click submit it will send the form data to the store post controller -->
</form>
@endsection
