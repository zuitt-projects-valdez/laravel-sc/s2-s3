@extends('layouts.app')

@section('content')
    @if(!Auth::guest())
    <!-- if the user is not a guest (if the user is logged in) -->
        @if(Auth::user()->id===$post->user_id)
        <!-- if the post's user id matches the currently logged in user; edit post page will depend on the id of the logged in user; only the user who owns the post will be able to access the post's edit page -->
            <h1>Edit Post</h1>
            <form action="{{action('PostController@update', [$post->id])}}" method='POST'>
            <!-- can't make it into a method = put; laravel doesn't do that 
            so we have to add a hidden input - method spoofing 
            -->
                @csrf
                <!-- hidden input to change the method to put  -->
                <input type="hidden" name='_method' value='PUT'>
                <!-- generates a CSRF token everytimee a form is submitted (security) -->
                <div class="form-group">
                    <label for="title-input">Title</label>
                    <input id='title-input' type="text" name='title' class='form-control' value='{{$post->title}}' >
                </div>
                <div class="form-group">
                    <label for="body-input">Body</label>
                    <textarea id="body-input" class='form-control' name='body' rows="5">{{$post->body}}</textarea>
                </div>

                <button type='submit' class='btn btn-primary'>Submit</button>
            
            </form>
        @else 
            <p>You cannot edit another user's post.</p>
            <a href="/posts">Go back</a>
        @endif
    @endif
@endsection
