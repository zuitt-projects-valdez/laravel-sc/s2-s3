<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->mediumText('body');
            $table->timestamps();
        });

        // unsignedBigInteger - data type 
        Schema::table('posts', function($table){
            $table->unsignedBigInteger('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('posts');

        // need to add the code below in case we ever want to reverse out migrations 
        // formulating a schema will indicate what you want to table to look like, this schema is creating a new column for the user id 
        Schema::table('posts', function($table){
            $table->dropColumn('user_id');
        });
    }
}
