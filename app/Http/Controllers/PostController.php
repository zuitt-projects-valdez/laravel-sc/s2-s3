<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\PostComment;
class PostController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
        //middleware to redirect not logged in users to the login page when clicking the create page
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $posts = Post:;all();
         //$posts = Post::orderBy('id','asc')->paginate(5);
         $posts = Post::orderBy('created_at','desc')->paginate(5);
        // $posts = Post::orderBy('title', 'desc')->get();
         //$posts = Post::orderBy('title', 'desc')->take(1)->get(); //will only get the first post with the take(1); so can limit the number of results you get
        // $posts = Post::where('title', 'Post two')->get();

        return view('posts.index')->with('posts', $posts);
        // index is showing all posts (that's why index page has a foreach to loop through and display all the posts)
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
        //create is responsible for showing the page but the store method receives the request or form data from create 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // receives the form data, will process saving the form data to the database
        // need to create validation of the request 
        $this->validate($request, [
            'title'=>'required',
            'body'=>'required',
        ]);

        //save form to db 
        $post = new Post; 
        $post->title = $request->input('title'); //input(body/title) comes from the name of the form input in the create page, the two have to be the same 
        $post->body = $request->input('body');
        // auth will check for the id of the CURRENTLY logged in user and save it to the user id column of the posts table 
        $post->user_id = auth()->user()->id;
        $post->save();

        return redirect('/posts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        $comments = PostComment::orderBy('created_at','desc')->get();
        // post variable reps the Post model finding the id of the post from the show($id) -- show knows to do this because of the templating in laravel (laravel knows what type of endpoint you are trying to access with the show method/controller); model is the middleman and it will run a find operation using an id from the url
        return view('posts.show')->with('post', $post)->with('comments', $comments);
        // returns the show page in the posts folder but it needs the data (title, timestamp, body) so need to pass to the page the data we got from the db -> with (passes the object data to the page)
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // find the post info so it can be in the input already 
        $post = Post::find($id);
        return view('posts.edit')->with('post', $post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'=>'required',
            'body'=>'required',
        ]);

        $post = Post::find($id);
        $post->title = $request->input('title'); 
        $post->body = $request->input('body');
        $post->save();

        return redirect('/home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $post->delete();

        return redirect('/home');
    }

    public function comment(Request $request, $id){
        $this->validate($request, [
            'content'=>'required'
        ]);
       
        $post = Post::find($id);
        $comment = new PostComment;
        $comment->post_id = $post->id;
        $comment->user_id = auth()->user()->id; 
        $comment->content = $request -> input('content');
        $comment->save();

        return redirect("/posts/$id");
    }
}
