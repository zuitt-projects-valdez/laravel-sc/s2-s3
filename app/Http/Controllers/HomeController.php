<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Post;
use App\User; 

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
 

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $posts = Post::where('user_id', auth()->user()->id)->get();
        // return view('home')->with('posts', $posts);
        // REFRACTORED: 
        $user_id = auth()->user()->id; //id of the logged in user 
        $user = User::find($user_id);
        return view('home')->with('posts', $user->posts);
        // there is no posts column in the users table; possible because we established the connection between the two models (model relationships)
    }
}
