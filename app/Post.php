<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //creating a two way relationship between users and posts, so users can have ownership over posts; one to many relationship (one author can have multiple posts, posts only have one author)

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function postcomment(){
        return $this->hasMany('App\PostComment');
    }
}
