<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'PageController@index');
// Route::get('/hello', 'PageController@hello');
Route::get('/about', 'PageController@about');
Route::get('/services', 'PageController@services');

Route::resource('/posts', 'PostController');

Route::post('/posts/{id}/comment', 'PostController@comment');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
